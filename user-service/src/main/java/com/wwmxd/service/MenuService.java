package com.wwmxd.service;


import com.wwmxd.entity.Menu;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author WWMXD
 * @since 2018-01-03 14:39:38
 */
public interface MenuService extends  IService<Menu> {
}
